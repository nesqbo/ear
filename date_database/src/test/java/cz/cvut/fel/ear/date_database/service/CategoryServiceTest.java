package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.dao.CategoryRepository;
import cz.cvut.fel.ear.date_database.model.Category;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static cz.cvut.fel.ear.date_database.testutils.Generator.generateCategory;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
@Slf4j
@ActiveProfiles("test")
class CategoryServiceTest {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    CategoryService categoryService;
    @Test
    public void findAllCategories() {
        Category category = generateCategory();
        categoryRepository.save(category);
        assertEquals(categoryService.findAllCategories().size(), 1);
    }

}