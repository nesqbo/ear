package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.dao.CategoryRepository;
import cz.cvut.fel.ear.date_database.dao.OwnerRepository;
import cz.cvut.fel.ear.date_database.dao.SpotRepository;
import cz.cvut.fel.ear.date_database.exception.InvalidValueInRequestBodyException;
import cz.cvut.fel.ear.date_database.model.Category;
import cz.cvut.fel.ear.date_database.model.Owner;
import cz.cvut.fel.ear.date_database.model.Spot;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.sql.SQLDataException;
import java.util.List;

import static cz.cvut.fel.ear.date_database.testutils.Generator.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
@ActiveProfiles("test")
class SpotServiceTest {
    @Autowired
    SpotService spotService;
    @Autowired
    SpotRepository spotRepository;
    @Autowired
    CategoryRepository categoryRepository;
    Owner owner;

    @BeforeEach
    void setUp(@Autowired OwnerRepository ownerRepository) {
        owner = generateOwner();
        ownerRepository.save(owner);
    }

    @Test
    void getAllSpots() {
        Spot spot1 = generateSpot(owner);
        Spot spot2 = generateSpot(owner);

        spotRepository.save(spot1);
        spotRepository.save(spot2);

        List<Spot> spots = spotService.getAllSpots();

        assertTrue(spots.contains(spot1));
        assertTrue(spots.contains(spot2));
    }

    @Test
    void getAllSpots_cantShowDeletedSpots() throws SQLDataException {
        Spot spot1 = generateSpot(owner);
        Spot spot2 = generateSpot(owner);

        spotRepository.save(spot1);
        spotRepository.save(spot2);

        spotService.deleteSpotById(spot1.getId());

        List<Spot> spots = spotService.getAllSpots();

        assertFalse(spots.contains(spot1));
        assertTrue(spots.contains(spot2));
    }

    @Test
    void addSpot() {
        Spot spot = generateSpot(owner);

        spotService.addSpot(spot);
        List<Spot> spots = spotService.getAllSpots();

        assertTrue(spots.contains(spot));
    }

    @Test
    void addSpot_nameTooShort() {
        Spot spot = generateSpot(owner);
        spot.setName("ha");

        InvalidValueInRequestBodyException exception =
                assertThrows(InvalidValueInRequestBodyException.class, () -> spotService.addSpot(spot));
        assertEquals("Name must be between 3 and 20 characters", exception.getMessage());


        List<Spot> spots = spotService.getAllSpots();
        assertFalse(spots.contains(spot));
    }

    @Test
    void addSpot_nameTooLong() {
        Spot spot = generateSpot(owner);
        spot.setName("a".repeat(21));

        InvalidValueInRequestBodyException exception =
                assertThrows(InvalidValueInRequestBodyException.class, () -> spotService.addSpot(spot));
        assertEquals("Name must be between 3 and 20 characters", exception.getMessage());

        List<Spot> spots = spotService.getAllSpots();
        assertFalse(spots.contains(spot));
    }

    @Test
    void addSpot_nameNotUnique() {
        Spot spot = generateSpot(owner);
        Spot spot2 = generateSpot(owner);
        spot2.setName(spot.getName());

        spotService.addSpot(spot);

        InvalidValueInRequestBodyException exception =
                assertThrows(InvalidValueInRequestBodyException.class, () -> spotService.addSpot(spot2));
        assertEquals("Spot with name " + spot2.getName() + " already exists", exception.getMessage());

        List<Spot> spots = spotService.getAllSpots();
        assertFalse(spots.contains(spot2));
    }

    @Test
    void addSpot_noDescription() {
        Spot spot = generateSpot(owner);
        spot.setDescription(null);

        InvalidValueInRequestBodyException exception =
                assertThrows(InvalidValueInRequestBodyException.class, () -> spotService.addSpot(spot));
        assertEquals("Description must be between 0 and 140 characters", exception.getMessage());

        List<Spot> spots = spotService.getAllSpots();
        assertFalse(spots.contains(spot));
    }

    @Test
    void addSpot_descriptionTooLong() {
        Spot spot = generateSpot(owner);
        spot.setDescription("a".repeat(141));

        InvalidValueInRequestBodyException exception =
                assertThrows(InvalidValueInRequestBodyException.class, () -> spotService.addSpot(spot));
        assertEquals("Description must be between 0 and 140 characters", exception.getMessage());

        List<Spot> spots = spotService.getAllSpots();
        assertFalse(spots.contains(spot));
    }


    @Test
    void getSpotById() {
        Spot spot = generateSpot(owner);
        spotService.addSpot(spot);

        Spot spotFromDB = spotRepository.getSpotById(spot.getId());

        assertEquals(spot, spotFromDB);
    }

    @Test
    void getSpotById_spotIsDeleted() throws SQLDataException {
        Spot spot = generateSpot(owner);
        spotService.addSpot(spot);
        spotService.deleteSpotById(spot.getId());

        InvalidValueInRequestBodyException exception =
                assertThrows(InvalidValueInRequestBodyException.class, () -> spotService.getSpotById(spot.getId()));
        assertEquals("Spot with id " + spot.getId() + " does not exist", exception.getMessage());
    }

    @Test
    void updateSpot() throws SQLDataException {
        Spot spot = generateSpot(owner);
        spotService.addSpot(spot);


        Spot spotFromDB = spotRepository.getSpotById(spot.getId());
        spotFromDB.setName("new name");
        spotService.updateSpot(spotFromDB);

        assertEquals("new name", spotRepository.getSpotById(spot.getId()).getName());
    }

    @Test
    void updateSpot_spotIsDeleted() throws SQLDataException {
        Spot spot = generateSpot(owner);
        spotService.addSpot(spot);
        spotService.deleteSpotById(spot.getId());

        spot.setName("new name");
        InvalidValueInRequestBodyException exception =
                assertThrows(InvalidValueInRequestBodyException.class, () -> spotService.updateSpot(spot));
        assertEquals("Spot does not exist", exception.getMessage());
    }

    @Test
    void deleteSpotById() throws SQLDataException {
        Spot spot = generateSpot(owner);
        spotService.addSpot(spot);

        spotService.deleteSpotById(spot.getId());

        assertTrue(spotRepository.getSpotById(spot.getId()).isDeleted());
    }

    @Test
    void addSpotToCategory() throws SQLDataException {
        Spot spot = generateSpot(owner);
        spotService.addSpot(spot);

        Category category = generateCategory();
        categoryRepository.save(category);

        spotService.addSpotToCategory(spot.getId(), category.getId());

        assertTrue(spotRepository.getSpotById(spot.getId()).getCategories().contains(category));
    }

    @Test
    void removeSpotFromCategory() throws SQLDataException {
        Spot spot = generateSpot(owner);
        spotService.addSpot(spot);

        Category category = generateCategory();
        categoryRepository.save(category);

        spotService.addSpotToCategory(spot.getId(), category.getId());
        assertTrue(spotRepository.getSpotById(spot.getId()).getCategories().contains(category));

        spotService.removeSpotFromCategory(spot.getId(), category.getId());
        assertFalse(spotRepository.getSpotById(spot.getId()).getCategories().contains(category));
    }

    @Test
    void removeSpotFromCategory_spotIsNotInCategory() {
        Spot spot = generateSpot(owner);
        spotService.addSpot(spot);

        Category category = generateCategory();
        categoryRepository.save(category);

        InvalidValueInRequestBodyException exception =
                assertThrows(InvalidValueInRequestBodyException.class, () -> spotService.removeSpotFromCategory(spot.getId(), category.getId()));
        assertEquals("Spot does not contain this category", exception.getMessage());
    }
}