package cz.cvut.fel.ear.date_database.dao;

import cz.cvut.fel.ear.date_database.model.Category;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
public class NamedCategoryRepositoryTest {


        @Mock
        private EntityManager em;

        @Mock
        private TypedQuery<Category> query;

        @InjectMocks
        private NamedCategoryRepository namedCategoryRepository;

        @BeforeEach
        void setUp() {
            MockitoAnnotations.openMocks(this);
        }

        @Test
        void testFindAll() {
            Category horrorCategory = new Category();
            horrorCategory.setName("horror");
            Category annimationCategory = new Category();
            annimationCategory.setName("annimation");


            List<Category> categories = Arrays.asList(horrorCategory, annimationCategory);
            when(em.createNamedQuery("Category.findAll", Category.class)).thenReturn(query);
            when(query.getResultList()).thenReturn(categories);


            List<Category> result = namedCategoryRepository.findAll();


            assertEquals(categories, result);
            verify(query, times(1)).getResultList();
        }

        @Test
        void testFindByName() {
            String categoryName = "TestCategory";
            Category category = new Category();
            category.setName(categoryName);
            List<Category> categories = Arrays.asList(new Category());
            when(em.createNamedQuery("Category.findByName", Category.class)).thenReturn(query);
            when(query.setParameter("name", categoryName)).thenReturn(query);
            when(query.getResultList()).thenReturn(categories);


            List<Category> result = namedCategoryRepository.findByName(categoryName);


            assertEquals(categories, result);
            verify(query, times(1)).setParameter("name", categoryName);
            verify(query, times(1)).getResultList();
        }

}
