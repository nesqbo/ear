package cz.cvut.fel.ear.date_database.testutils;

import cz.cvut.fel.ear.date_database.model.*;

import java.util.Date;
import java.util.Random;

import static cz.cvut.fel.ear.date_database.service.utils.UtilMethods.encodePassword;

public class Generator {
    private static final Random random = new Random();
    public static BaseUser generateUser() {
        BaseUser user = new BaseUser();
        user.setUsername("test" + random.nextInt());
        user.setPassword("test");
        user.setEmail("test" + random.nextInt() + "@test.com");
        user.setAccountCreation(new Date());
        return user;
    }

    public static Owner generateOwner() {
        Owner owner = new Owner();
        owner.setUsername("test" + random.nextInt());
        owner.setPassword("test");
        owner.setEmail("test" + random.nextInt() + "@test.com");
        owner.setAccountCreation(new Date());
        owner.setIco(generateICO());
        owner.setName("test" + random.nextInt());
        return owner;
    }

    public static Admin generateAdmin(Admin.AdminLevel level) {
        Admin admin = new Admin();
        admin.setUsername("test" + random.nextInt());
        admin.setPassword(encodePassword("test"));
        admin.setEmail("test" + random.nextInt() + "@test.com");
        admin.setAccountCreation(new Date());
        admin.setAdminLevel(level);
        return admin;
    }

    public static String generateICO() {
        StringBuilder ICO = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            ICO.append(random.nextInt(10));
        }
        return ICO.toString();
    }

    public static Review generateReview(Spot spot, BaseUser user) {
        Review review = new Review();
        review.setRating(random.nextInt(1, 5));
        review.setTitle("test" + random.nextInt());
        review.setContent("test");
        review.setDate(new Date());
        review.setDeleted(false);
        review.setReportsReceived(0);
        review.setSpot(spot);
        review.setUser(user);

        return review;
    }

    public static Spot generateSpot(Owner owner){
        Spot spot = new Spot();
        spot.setName("test" + random.nextInt());
        spot.setOwner(owner);
        String coordinates = random.nextFloat() + "," + random.nextFloat();
        spot.setCoordinates(coordinates);
        spot.setDescription("test");
        return spot;
    }

    public static Category generateCategory() {
        Category category = new Category();
        category.setName("test" + random.nextInt());
        return category;
    }
}
