package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.dao.*;
import cz.cvut.fel.ear.date_database.model.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.ear.date_database.testutils.Generator.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AdminActionsServiceTest {
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private AdminActionsService adminActionsService;
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SpotRepository spotRepository;
    @Autowired
    private OwnerRepository ownerRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    private BaseUser user;
    private Owner owner;
    private Spot spot;




    @BeforeEach
    public void setUp() {
        user = generateUser();
        owner = generateOwner();
        spot = generateSpot(owner);
        userRepository.save(user);
        ownerRepository.save(owner);
        spotRepository.save(spot);
    }

    @Test
    public void deleteReview() {
        Admin admin = generateAdmin(Admin.AdminLevel.REVIEW_MANAGEMENT);
        adminRepository.save(admin);

        Review review = generateReview(spot, user);
        reviewRepository.save(review);
        review = reviewRepository.findById(review.getId()).get();

        adminActionsService.deleteReview(review.getId());

        if (reviewRepository.findById(review.getId()).isPresent()) {
            assertTrue(reviewRepository.findById(review.getId()).get().isDeleted());
        } else {
            fail("Review was not found");
        }
    }

    @Test
    public void deleteUser() {
        user.setReportsReceived(50);
        userRepository.save(user);

        adminActionsService.deleteUser(user.getUsername());

        if (userRepository.findById(user.getId()).isPresent()) {
            assertTrue(userRepository.findById(user.getId()).get().isDeleted());
        } else {
            fail("User was not found");
        }
    }

    @Test
    public void deleteUser_notEnoughReports() {
        user.setReportsReceived(49);
        userRepository.save(user);


        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                adminActionsService.deleteUser(user.getUsername()));

        assertEquals("User " + user.getUsername() + " has not received enough reports to be deleted", exception.getMessage());

        if (userRepository.findById(user.getId()).isPresent()) {
            assertFalse(userRepository.findById(user.getId()).get().isDeleted());
        } else {
            fail("User was not found");
        }
    }

    @Test
    public void deleteSpot() {
        adminActionsService.deleteSpot(spot.getId());

        if (spotRepository.findById(spot.getId()).isPresent()) {
            assertTrue(spotRepository.findById(spot.getId()).get().isDeleted());
        } else {
            fail("Spot was not found");
        }
    }

    @Test
    public void deleteOwner() {
        Admin admin = generateAdmin(Admin.AdminLevel.SUPER_ADMIN);
        adminRepository.save(admin);

        owner.setReportsReceived(50);
        ownerRepository.save(owner);

        adminActionsService.deleteOwner(owner.getUsername());

        if (ownerRepository.findById(owner.getId()).isPresent()) {
            assertTrue(ownerRepository.findById(owner.getId()).get().isDeleted());
        } else {
            fail("Owner was not found");
        }
    }

    @Test
    public void deleteOwner_notEnoughReports() {
        owner.setReportsReceived(49);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                adminActionsService.deleteOwner(owner.getUsername()));

        assertEquals("Owner " + owner.getUsername() + " has not received enough reports to be deleted", exception.getMessage());

        if (ownerRepository.findById(owner.getId()).isPresent()) {
            assertFalse(ownerRepository.findById(owner.getId()).get().isDeleted());
        } else {
            fail("Owner was not found");
        }
    }

    @Test
    public void deleteUserById_deletesAllUsersReviews() {
        user.setReportsReceived(71);
        userRepository.save(user);

        Review review = generateReview(spot, user);
        reviewRepository.save(review);
        assertEquals(1, reviewRepository.findAllByUser(user).size());


        adminActionsService.deleteUser(user.getUsername());

        if (userRepository.findById(user.getId()).isPresent()) {
            assertTrue(userRepository.findById(user.getId()).get().isDeleted());
            assertTrue(reviewRepository.findAllByUser(user).get(0).isDeleted());
        } else {
            fail("User was not found");
        }
    }

    @Test
    public void deleteOwnerById_deletesOwnersSpots(){
        owner.setReportsReceived(50);
        ownerRepository.save(owner);
        Spot spot = generateSpot(owner);
        spotRepository.save(spot);

        adminActionsService.deleteOwner(owner.getUsername());


        if (ownerRepository.findById(owner.getId()).isPresent() && spotRepository.findById(spot.getId()).isPresent()) {
            assertTrue(ownerRepository.findById(owner.getId()).get().isDeleted());
            assertTrue(spotRepository.findById(spot.getId()).get().isDeleted());
        } else {
            fail("Owner was not found");
        }
    }

    @Test
    void addCategory() {
        Category category = generateCategory();
        adminActionsService.addCategory(category);

        assertTrue(categoryRepository.existsByName(category.getName()));
    }

    @Test
    void addCategory_duplicateName() {
        Category category = generateCategory();
        adminActionsService.addCategory(category);

        Category category1 = generateCategory();
        category1.setName(category.getName());

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                adminActionsService.addCategory(category1));
        assertEquals("Category with name " + category.getName() + " already exists", exception.getMessage());
    }

    @Test
    void deleteCategory() {
        Category category = generateCategory();
        categoryRepository.save(category);

        adminActionsService.deleteCategory(category.getId());

        if (categoryRepository.findById(category.getId()).isPresent()) {
            assertTrue(categoryRepository.findById(category.getId()).get().isDeleted());
        } else {
            fail("Category was not found");
        }
    }

    @Test
    void deleteCategory_alreadyDeleted() {
        Category category = generateCategory();
        category.setDeleted(true);
        categoryRepository.save(category);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                adminActionsService.deleteCategory(category.getId()));
        assertEquals("Category with id " + category.getId() + " is already deleted", exception.getMessage());
    }

    @Test
    void deleteCategory_notFound() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                adminActionsService.deleteCategory(0));
        assertEquals("Category with id 0 does not exist", exception.getMessage());
    }

    @Test
    void deleteCategory_deletesFromSpotCategories() {
        Category category = generateCategory();
        categoryRepository.save(category);
        List<Category> categories = new ArrayList<>(spot.getCategories());
        categories.add(category);
        spot.setCategories(categories);
        spotRepository.save(spot);

        adminActionsService.deleteCategory(category.getId());

        if (categoryRepository.findById(category.getId()).isPresent() && spotRepository.findById(spot.getId()).isPresent()) {
            assertTrue(categoryRepository.findById(category.getId()).get().isDeleted());
            assertFalse(spotRepository.findById(spot.getId()).get().getCategories().contains(category));
        } else {
            fail("Category was not found");
        }
    }
}