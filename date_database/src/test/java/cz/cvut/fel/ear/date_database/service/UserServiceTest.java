package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.dao.AdminRepository;
import cz.cvut.fel.ear.date_database.dao.OwnerRepository;
import cz.cvut.fel.ear.date_database.dao.UserRepository;
import cz.cvut.fel.ear.date_database.exception.DateDBException;
import cz.cvut.fel.ear.date_database.exception.InvalidIcoException;
import cz.cvut.fel.ear.date_database.model.Admin;
import cz.cvut.fel.ear.date_database.model.BaseUser;
import cz.cvut.fel.ear.date_database.model.Owner;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static cz.cvut.fel.ear.date_database.testutils.Generator.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserServiceTest {
    @Autowired UserService userService;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    OwnerRepository ownerRepository;
    BaseUser user;
    Owner owner;
    Admin admin;
    @BeforeEach
    void setUp() {
        user = generateUser();
        owner = generateOwner();
        admin = generateAdmin(Admin.AdminLevel.REVIEW_MANAGEMENT);

        userRepository.save(user);
        ownerRepository.save(owner);
        adminRepository.save(admin);
    }

    @Test
    void getAllUsersOfAllTypes() {
        List<BaseUser> allUsers = userService.getAllUsersOfAllTypes();

        assertTrue(allUsers.contains(user));
        assertTrue(allUsers.contains(owner));
        assertTrue(allUsers.contains(admin));
    }

    @Test
    void getAllAdmins() {
        List<Admin> allAdmins = userService.getAllAdmins();
        assertTrue(allAdmins.contains(admin));
        assertFalse(allAdmins.contains(owner));
        assertFalse(allAdmins.contains(user));
    }

    @Test
    void getAllOwners() {
        List<Owner> allOwners = userService.getAllOwners();
        assertTrue(allOwners.contains(owner));
        assertFalse(allOwners.contains(admin));
        assertFalse(allOwners.contains(user));
    }

    @Test
    void getAllUsers() {
        //We want owners and admins to be able to login to our application as base users using the same credentials
        List<BaseUser> allUsers = userService.getAllBaseUsers();

        assertTrue(allUsers.contains(user));
        assertTrue(allUsers.contains(owner));
        assertTrue(allUsers.contains(admin));
    }

    @Test
    void addOwner() {
        Owner owner = generateOwner();
        Owner owner2 = generateOwner();
        Owner owner3 = generateOwner();

        userService.addOwner(owner);
        userService.addOwner(owner2);
        userService.addOwner(owner3);

        List<Owner> allOwners = userService.getAllOwners();

        assertTrue(allOwners.contains(owner));
        assertTrue(allOwners.contains(owner2));
        assertTrue(allOwners.contains(owner3));
        assertFalse(allOwners.contains(user));
        assertFalse(allOwners.contains(admin));
    }

    @Test
    void addOwner_invalidICO_wrongIcoLength() {
        Owner owner = generateOwner();
        owner.setIco("123456789");

        InvalidIcoException exception = assertThrows(InvalidIcoException.class, () -> userService.addOwner(owner));
        assertEquals("ICO must be 8 characters long", exception.getMessage());
    }

    @Test
    void addOwner_invalidICO_notUniqueIco() {
        Owner owner = generateOwner();
        owner.setIco("12345678");

        Owner owner2 = generateOwner();
        owner2.setIco("12345678");

        userService.addOwner(owner);

        InvalidIcoException exception = assertThrows(InvalidIcoException.class, () -> userService.addOwner(owner2));
        assertEquals("ICO must be unique", exception.getMessage());
    }

    @Test
    void addOwner_invalidICO_icoIsNotNumeric() {
        Owner owner = generateOwner();
        owner.setIco("test1234");

        InvalidIcoException exception = assertThrows(InvalidIcoException.class, () -> userService.addOwner(owner));
        assertEquals("ICO must be a number", exception.getMessage());
    }

    @Test
    void onlyAdminCanViewUsersReportsReceived() {//happy flow test
        BaseUser baseUser = generateUser();
        baseUser.setReportsReceived(70);

        userService.addUser(baseUser);

        assert(userService.viewUsersReportsReceived(baseUser.getId()) == 70);
    }

    @Test
    void addUser() {
        BaseUser user = generateUser();
        userService.addUser(user);

        List<BaseUser> allUsers = userRepository.findAll();
        assertTrue(allUsers.contains(user));
    }

    @Test
    void addUser_usernameTaken() {
        BaseUser newUser = generateUser();
        newUser.setUsername(user.getUsername());

        DateDBException exception = assertThrows(DateDBException.class, () -> userService.addUser(newUser));
        assertEquals("Username must be unique", exception.getMessage());

        List<BaseUser> allUsers = userRepository.findAll();
        assertFalse(allUsers.contains(newUser));
    }

    @Test
    void addUser_emailTaken() {
        BaseUser newUser = generateUser();
        newUser.setEmail(user.getEmail());

        DateDBException exception = assertThrows(DateDBException.class, () -> userService.addUser(newUser));
        assertEquals("Email must be unique", exception.getMessage());

        List<BaseUser> allUsers = userRepository.findAll();
        assertFalse(allUsers.contains(newUser));
    }

    @Test
    void addUser_invalidEmailFormat() {
        BaseUser newUser = generateUser();
        newUser.setEmail("testfailmail");

        DateDBException exception = assertThrows(DateDBException.class, () -> userService.addUser(newUser));
        assertEquals("Email must be valid", exception.getMessage());

        List<BaseUser> allUsers = userRepository.findAll();
        assertFalse(allUsers.contains(newUser));
    }


    @Test
    void addUser_usernameTooShort() {
        BaseUser newUser = generateUser();
        newUser.setUsername("ha");

        DateDBException exception = assertThrows(DateDBException.class, () -> userService.addUser(newUser));
        assertEquals("Username must be between 3 and 20 characters", exception.getMessage());

        List<BaseUser> allUsers = userRepository.findAll();
        assertFalse(allUsers.contains(newUser));
    }

    @Test
    void addUser_usernameTooLong() {
        BaseUser newUser = generateUser();
        newUser.setUsername("a".repeat(21));

        DateDBException exception = assertThrows(DateDBException.class, () -> userService.addUser(newUser));
        assertEquals("Username must be between 3 and 20 characters", exception.getMessage());

        List<BaseUser> allUsers = userRepository.findAll();
        assertFalse(allUsers.contains(newUser));
    }

    @Test
    void addAdmin() {
        Admin admin = generateAdmin(Admin.AdminLevel.REVIEW_MANAGEMENT);
        userService.addAdmin(admin);

        List<Admin> allAdmins = adminRepository.findAll();
        assertTrue(allAdmins.contains(admin));
    }

    @Test
    void addAdmin_nullAdminLevel() {
        Admin admin = generateAdmin(null);

        DateDBException exception = assertThrows(DateDBException.class, () -> userService.addAdmin(admin));
        assertEquals("Admin level must be set", exception.getMessage());

        List<Admin> allAdmins = adminRepository.findAll();
        assertFalse(allAdmins.contains(admin));
    }

    @Test
    void viewUsersReportsReceived() {
        BaseUser user = generateUser();
        user.setReportsReceived(50);
        userRepository.save(user);

        assertEquals(50, userService.viewUsersReportsReceived(user.getId()));
    }
    @Test
    void changePassword() {
        userService.changePassword(user, "test", "newPassword");
        assertEquals("newPassword", user.getPassword());
    }

    @Test
    void changePassword_wrongCredentials() {
        DateDBException exception = assertThrows(DateDBException.class, () -> userService.changePassword(user, "wrong", "newPassword"));
        assertEquals("wrong credentials", exception.getMessage());
    }

    @Test
    void changePassword_sameOldPasswordAsNew() {
        DateDBException exception = assertThrows(DateDBException.class, () -> userService.changePassword(user, "test", "test"));
        assertEquals("New password must be different from the old one", exception.getMessage());
    }

    @Test
    void changePassword_nullOldPassword() {
        DateDBException exception = assertThrows(DateDBException.class, () -> userService.changePassword(user, null, "newPassword"));
        assertEquals("Old and new password must be specified", exception.getMessage());
    }

    @Test
    void changePassword_nullNewPassword() {
        DateDBException exception = assertThrows(DateDBException.class, () -> userService.changePassword(user, "test", null));
        assertEquals("Old and new password must be specified", exception.getMessage());
    }
}