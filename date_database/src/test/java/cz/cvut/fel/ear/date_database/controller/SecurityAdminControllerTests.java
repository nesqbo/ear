package cz.cvut.fel.ear.date_database.controller;

import cz.cvut.fel.ear.date_database.dao.SpotRepository;
import cz.cvut.fel.ear.date_database.model.Admin;
import cz.cvut.fel.ear.date_database.security.SecurityConfig;
import cz.cvut.fel.ear.date_database.service.AdminActionsService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static cz.cvut.fel.ear.date_database.testutils.Generator.generateAdmin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@ContextConfiguration(classes = {SecurityAdminControllerTests.class, SecurityConfig.class})
public class SecurityAdminControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AdminActionsService adminService;

    @Autowired
    private SpotRepository spotRepository;

    private Admin admin;

    @BeforeEach
    public void setUp(){
        this.admin = generateAdmin(Admin.AdminLevel.SPOT_MANAGEMENT);
    }

    @AfterEach
    public void tearDown(){
        spotRepository.deleteAll();
        Mockito.reset(adminService);
    }

    @WithAnonymousUser
    @Test
    public void deleteSpotThrowsUnauthorized() throws Exception {
        mockMvc.perform(delete("/v1/api/admin/spot/1")
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }
}
