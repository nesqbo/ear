package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.dao.OwnerRepository;
import cz.cvut.fel.ear.date_database.dao.ReviewRepository;
import cz.cvut.fel.ear.date_database.dao.SpotRepository;
import cz.cvut.fel.ear.date_database.dao.UserRepository;
import cz.cvut.fel.ear.date_database.exception.InvalidReviewException;
import cz.cvut.fel.ear.date_database.exception.UnauthorizedException;
import cz.cvut.fel.ear.date_database.model.BaseUser;
import cz.cvut.fel.ear.date_database.model.Owner;
import cz.cvut.fel.ear.date_database.model.Review;
import cz.cvut.fel.ear.date_database.model.Spot;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;
import java.util.List;

import static cz.cvut.fel.ear.date_database.service.utils.UtilMethods.addMinutes;
import static cz.cvut.fel.ear.date_database.testutils.Generator.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
@ActiveProfiles("test")
public class ReviewServiceTest {

    @Autowired
    ReviewService reviewService;
    @Autowired
    UserService userService;
    @Autowired
    SpotService spotService;
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    OwnerRepository ownerRepository;
    @Autowired
    SpotRepository spotRepository;
    @Autowired
    UserRepository userRepository;
    Review review;

    @BeforeEach
    public void setUp() {
        Owner owner = generateOwner();
        Spot spot = generateSpot(owner);
        BaseUser baseUser = generateUser();
        review = generateReview(spot, baseUser);

        ownerRepository.save(owner);
        spotRepository.save(spot);
        userRepository.save(baseUser);
    }

    //todo add all missing assertions - all work
    @Test
    void reviewPlace() {//happy flow
        BaseUser baseUser = generateUser();
        Owner owner = generateOwner();
        Spot spot = generateSpot(owner);
        userService.addUser(baseUser);

        userService.addOwner(owner);
        spotService.addSpot(spot);

        Review newReview = new Review(1 ,"title", "content", 5, new Date(), false, 0, baseUser, spot);

        reviewService.reviewPlace(newReview);
        assertEquals(1, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
        assertEquals(5, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).get(0).getRating());
        assertEquals("title", reviewService.getAllReviewsBasedOnSpotId(spot.getId()).get(0).getTitle());
        assertEquals("content", reviewService.getAllReviewsBasedOnSpotId(spot.getId()).get(0).getContent());
    }

    @Test
    void reviewPlace_MoreThanOnceWithin3Months() throws UnauthorizedException, InvalidReviewException {//happy flow
        BaseUser baseUser = generateUser();
        Owner owner = generateOwner();
        Spot spot = generateSpot(owner);
        userService.addUser(baseUser);
        userService.addOwner(owner);
        spotService.addSpot(spot);


        //first review
        Review newReview1 = new Review(1 ,"first", "first", 5, new Date(), false, 0, baseUser, spot);

        reviewService.reviewPlace(newReview1);

        //second review right after should throw an exception
        Review newReview2 = new Review(1 ,"first", "first", 1, new Date(), false, 0, baseUser, spot);

        assertThrows(InvalidReviewException.class, () -> reviewService.reviewPlace(newReview2));
        List<Review> allReviewsBasedOnSpotId = reviewService.getAllReviewsBasedOnSpotId(spot.getId());
        assertEquals (1, allReviewsBasedOnSpotId.size()); // most important assertion
        assertEquals(5, allReviewsBasedOnSpotId.get(0).getRating());
        assertEquals("first", allReviewsBasedOnSpotId.get(0).getTitle());
        assertEquals("first", allReviewsBasedOnSpotId.get(0).getContent());

    }

    //todo - from here on down, tests havent been finished, to be finished
//    @Test
//    void reviewPlace_After3Months() throws UnauthorizedException, IllegalArgumentException{//not happy flow
//        BaseUser baseUser = generateUser();
//        Owner owner = generateOwner();
//        Spot spot = generateSpot(owner1);
//        userService.addUser(baseUser);
//
//        userService.addOwner(owner);
//
//        spotService.addSpot(spot);
//
//        reviewService.reviewPlace(baseUser.getId(), spot.getId(), 5, "title", "content");
//        assertEquals(reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size() == 1);
//        assertEquals(reviewService.getAllReviewsBasedOnSpotId(spot.getId()).get(0).getRating() == 5);
//        assertEquals(reviewService.getAllReviewsBasedOnSpotId(spot.getId()).get(0).getTitle().equals("title"));
//        assertEquals(reviewService.getAllReviewsBasedOnSpotId(spot.getId()).get(0).getContent().equals("content"));
//    }

    @Test
    void reviewPlace_WithRatingLessThan1() throws UnauthorizedException, InvalidReviewException{//not happy flow
        BaseUser baseUser = generateUser();
        Owner owner = generateOwner();
        Spot spot = generateSpot(owner);
        userService.addUser(baseUser);

        userService.addOwner(owner);
        spotService.addSpot(spot);
        Review newReview = new Review(1 ,"first", "first", -1, new Date(), false, 0, baseUser, spot);
        assertThrows(InvalidReviewException.class, () -> reviewService.reviewPlace(newReview));
        //todo check if the review was not added
    }

    @Test
    void reviewPlace_WithRatingMoreThan5() throws UnauthorizedException, InvalidReviewException{//not happy flow
        BaseUser baseUser = generateUser();
        Owner owner = generateOwner();
        Spot spot = generateSpot(owner);
        userService.addUser(baseUser);

        userService.addOwner(owner);
        spotService.addSpot(spot);

        Review newReview = new Review(1 ,"first", "first", 7, new Date(), false, 0, baseUser, spot);
        //todo check if the review was not added
        assertThrows(InvalidReviewException.class, () -> reviewService.reviewPlace(newReview));
    }

    @Test
    void reviewPlace_twoDifferentReviewers(){//happy flow
        BaseUser baseUser1 = generateUser();
        BaseUser baseUser2 = generateUser();
        Owner owner1 = generateOwner();
        Spot spot = generateSpot(owner1);
        userService.addUser(baseUser2);
        userService.addUser(baseUser1);

        userService.addOwner(owner1);
        spot.setOwner(owner1);
        spotService.addSpot(spot);

        Review newReview1 = new Review(1 ,"first", "first", 1, new Date(), false, 0, baseUser1, spot);
        Review newReview2 = new Review(2 ,"second", "second", 5, new Date(), false, 0, baseUser2, spot);

        reviewService.reviewPlace(newReview1);
        reviewService.reviewPlace(newReview2);
        List<Review> reviews = reviewService.getAllReviewsBasedOnSpotId(spot.getId());
        assertEquals(2, reviews.size());

        assertEquals(1, reviews.get(0).getRating());
        assertEquals("first", reviews.get(0).getTitle());
        assertEquals("first", reviews.get(0).getContent());

        assertEquals(5, reviews.get(1).getRating() );
        assertEquals("second", reviews.get(1).getTitle());
        assertEquals("second", reviews.get(1).getContent());
    }

    @Test
    void calculateAverageSpotReview() {
        BaseUser baseUser1 = generateUser();
        BaseUser baseUser2 = generateUser();
        Owner owner1 = generateOwner();
        Spot spot = generateSpot(owner1);
        userService.addUser(baseUser2);
        userService.addUser(baseUser1);

        userService.addOwner(owner1);
        spotService.addSpot(spot);


        Review newReview = new Review(1 ,"first", "first", 1, new Date(), false, 0, baseUser2, spot);
        reviewService.reviewPlace(newReview);
        spot = spotService.getSpotById(spot.getId());
        assertEquals(reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size(), 1);
        assertEquals(1.0, spotService.getSpotById(spot.getId()).getReviewAverage());
        Review newReview2 = new Review(2 ,"first", "first", 5, new Date(), false, 0, baseUser1, spot);
        reviewService.reviewPlace(newReview2);
        assertEquals(reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size(), 2);
        assertEquals(3.0, spotService.getSpotById(spot.getId()).getReviewAverage());
    }

    @Test
    void calculateAverageSpotReview_With11Reviews() {
        BaseUser eleventh_user = generateUser();
        userService.addUser(eleventh_user);
        Owner owner1 = generateOwner();
        Spot spot = generateSpot(owner1);

        userService.addOwner(owner1);
        spotService.addSpot(spot);

        for (int i = 0; i < 10; i++){
            BaseUser user = generateUser();
            userService.addUser(user);
            Review newReview = new Review(i + 1 ,"first", "first", 1, new Date(), false, 0, user, spot);
            reviewService.reviewPlace(newReview);
        }
        assertEquals(10, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
        assertEquals(1.0, spotService.getSpotById(spot.getId()).getReviewAverage());

        //adding the 11th review which shouldnt change the previous average rating
        Review newReview = new Review(11 ,"first", "first", 5, new Date(), false, 0, eleventh_user, spot);
        reviewService.reviewPlace(newReview);
        assertEquals(11, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
        assertEquals(1.0, spotService.getSpotById(spot.getId()).getReviewAverage());
    }

    @Test
    void calculateAverageSpotReview_With20Reviews() {
        Owner owner1 = generateOwner();
        Spot spot = generateSpot(owner1);

        userService.addOwner(owner1);
        spotService.addSpot(spot);

        for (int i = 0; i < 10; i++){
            BaseUser user = generateUser();
            userService.addUser(user);
            Review newReview = new Review(i +1,"first", "first", 1, new Date(), false, 0, user, spot);
            reviewService.reviewPlace(newReview);
        }
        assertEquals(10, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
        assertEquals(1.0, spotService.getSpotById(spot.getId()).getReviewAverage());

        for (int i = 0; i < 10; i++) {
            BaseUser user = generateUser();
            userService.addUser(user);
            Review newReview = new Review(10+i+1 ,"first", "first", 5, new Date(), false, 0, user, spot);
            reviewService.reviewPlace(newReview);
        }
        assertEquals(20, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
        assertEquals(3.0, spotService.getSpotById(spot.getId()).getReviewAverage());
    }

    @Test
    void deleteReviewById(){
        BaseUser baseUser2 = generateUser();
        userService.addUser(baseUser2);

        Owner owner1 = generateOwner();
        Spot spot = generateSpot(owner1);
        userService.addOwner(owner1);
        spotService.addSpot(spot);
        Review newReview = new Review(1 ,"first", "first", 1, new Date(), false, 0, baseUser2, spot);


        reviewService.reviewPlace(newReview);
        Review review = reviewService.getAllReviewsBasedOnSpotId(spot.getId()).get(0);
        spot = spotService.getSpotById(spot.getId());
        assertEquals(1, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
        assertEquals(1.0, spotService.getSpotById(spot.getId()).getReviewAverage());
        reviewService.deleteReviewById(review.getId());
        assertEquals(0, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
        assertEquals(0.0, spotService.getSpotById(spot.getId()).getReviewAverage());
    }

//    @Test
//    void deleteReviewById_differentUserThanCreatingOne(){//not happy flow - a different user than the one who created the review is trying to delete it
//        BaseUser baseUser1 = generateUser();
//        BaseUser baseUser2 = generateUser();
//        Owner owner1 = generateOwner();
//        Spot spot = generateSpot(owner1);
//        userService.addUser(baseUser2);
//        userService.addUser(baseUser1);
//
//        userService.addOwner(owner1);
//        spotService.addSpot(spot);
//
//        //id, title, content, rating, date, deleted, reportsReceived, user, spot
//        Review review = new Review(1 ,"first", "first", 1, new Date(), false, 0, baseUser2, spot);
//
//
//
//        reviewService.reviewPlace(review);
//        Review reviewFromDB = reviewService.getAllReviewsBasedOnSpotId(spot.getId()).get(0);
//        spot = spotService.getSpotById(spot.getId());
//        assertEquals(1, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
//        assertEquals(1.0, spotService.getSpotById(spot.getId()).getReviewAverage());
//        assertThrows(UnauthorizedException.class, () -> reviewService.deleteReviewById(reviewFromDB.getId(), baseUser1.getId()));
//    }

    @Test
    void createReview_InvalidTime(){//BR - test should fail
        BaseUser baseUser = generateUser();
        Owner owner = generateOwner();
        Spot spot = generateSpot(owner);
        userService.addUser(baseUser);

        userService.addOwner(owner);
        spotService.addSpot(spot);

        Review newReview = new Review(1 ,"first", "first", 1, addMinutes(new Date(), 10), false, 0, baseUser, spot);

        assertThrows(InvalidReviewException.class, () -> reviewService.reviewPlace(newReview));

    }

    @Test
    void reportReview(){//happy flow
        BaseUser baseUser = generateUser();
        Owner owner = generateOwner();
        Spot spot = generateSpot(owner);
        userService.addUser(baseUser);

        userService.addOwner(owner);
        spotService.addSpot(spot);

        Review newReview = new Review(1 ,"first", "first", 1, new Date(), false, 0, baseUser, spot);
        reviewService.reviewPlace(newReview);
        reviewService.reportReview(newReview.getId());
        assertEquals(1, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
        assertEquals(1, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).get(0).getReportsReceived());

    }

    @Test
    void reportReview_thatIsDeleted(){//happy flow
        BaseUser baseUser = generateUser();
        Owner owner = generateOwner();
        Spot spot = generateSpot(owner);
        userService.addUser(baseUser);

        userService.addOwner(owner);
        spotService.addSpot(spot);

        Review newReview = new Review(1 ,"first", "first", 1, new Date(), false, 0, baseUser, spot);
        reviewService.reviewPlace(newReview);
        reviewService.deleteReviewById(newReview.getId());

        assertThrows(InvalidReviewException.class, () -> reviewService.reportReview(newReview.getId()));
        assertEquals(0, reviewService.getAllReviewsBasedOnSpotId(spot.getId()).size());
        assertEquals(0, reviewRepository.findReviewsBySpotIdIncludeDeleted(spot.getId()).get(0).getReportsReceived());
    }

    @Test
    void reportReview_50thReport(){//happy flow
        BaseUser baseUser = generateUser();
        Owner owner = generateOwner();
        Spot spot = generateSpot(owner);
        userService.addUser(baseUser);

        userService.addOwner(owner);
        spotService.addSpot(spot);

        Review newReview = new Review(1 ,"first", "first", 1, new Date(), false, 0, baseUser, spot);
        reviewService.reviewPlace(newReview);

        for (int i = 0; i < 50; i++){
            reviewService.reportReview(newReview.getId());
        }
        InvalidReviewException exception = assertThrows(InvalidReviewException.class, () -> reviewService.reportReview(newReview.getId()));
        assertEquals("Review is already deleted", exception.getMessage());
    }

    @Test
    void updateReview() {
        reviewRepository.save(review);
        review.setTitle("new title");
        review.setContent("new content");
        review.setRating(5);

        reviewService.updateReview(review);

        if (reviewRepository.findById(review.getId()).isPresent()) {
            assertEquals("new title", reviewRepository.findById(review.getId()).get().getTitle());
            assertEquals("new content", reviewRepository.findById(review.getId()).get().getContent());
            assertEquals(5, reviewRepository.findById(review.getId()).get().getRating());
        } else {
            fail("Review was not found");
        }
    }

    @Test
    void getAllReviewsBasedOnSpotId() {
        BaseUser baseUser1 = generateUser();
        userRepository.save(baseUser1);
        Review review1 = generateReview(review.getSpot(), baseUser1);
        reviewService.reviewPlace(review);
        reviewService.reviewPlace(review1);

        List<Review> reviews = reviewService.getAllReviewsBasedOnSpotId(review.getSpot().getId());
        assertTrue(reviews.contains(review));
        assertTrue(reviews.contains(review1));
    }

    @Test
    void getAllReviewsBasedOnSpotId_spotDoesntExist() {
        reviewService.reviewPlace(review);

    }
}
