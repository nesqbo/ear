package cz.cvut.fel.ear.date_database.dao;

import cz.cvut.fel.ear.date_database.model.Owner;
import cz.cvut.fel.ear.date_database.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static cz.cvut.fel.ear.date_database.testutils.Generator.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CriteriaOwnerTest {

    @Autowired
    private CriteriaOwner ownerRepository;
    @Autowired
    private UserService userService;
    Owner owner2, owner3;

    @BeforeAll
    void setUp() {
        owner2 = generateOwner();
        owner3 = generateOwner();

        userService.addOwner(owner2);
        userService.addOwner(owner3);
    }

    @Test
    void findOwnerByCriteriaFindByIcoTest() {

        List<Owner> allOwners = userService.getAllOwners();
        Owner returnedOwner2 = ownerRepository.findByIco(owner2.getIco());
        Owner returnedOwner3 = ownerRepository.findByIco(owner3.getIco());


        assertTrue(allOwners.contains(owner2));
        assertTrue(allOwners.contains(owner3));
        assertEquals(2, allOwners.size());
        assertEquals(owner2.getIco(), allOwners.get(0).getIco());
        assertEquals(owner3.getIco(), allOwners.get(1).getIco());

        assertEquals(owner2.getIco(), returnedOwner2.getIco());
        assertEquals(owner3.getIco(), returnedOwner3.getIco());
    }

    @Test
    public void findOwnerByCriteriaFindByUsernameTest() {
        List<Owner> allOwners = userService.getAllOwners();
        Optional<Owner> returnedOwner2 = ownerRepository.findByUsername(owner2.getUsername());
        Optional<Owner> returnedOwner3 = ownerRepository.findByUsername(owner3.getUsername());


        assertTrue(allOwners.contains(owner2));
        assertTrue(allOwners.contains(owner3));
        assertEquals(2, allOwners.size());

        if (returnedOwner2.isPresent() && returnedOwner3.isPresent()) {
            assertEquals(owner2.getUsername(), returnedOwner2.get().getUsername());
            assertEquals(owner3.getUsername(), returnedOwner3.get().getUsername());
        } else {
            fail();
        }
    }
}
