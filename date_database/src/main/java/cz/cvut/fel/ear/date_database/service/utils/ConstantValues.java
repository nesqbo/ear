package cz.cvut.fel.ear.date_database.service.utils;

public class ConstantValues {
    public static int TITLE_MAX_LENGTH = 50;
    public static int CONTENT_MAX_LENGTH = 500;
    public static float RATING_MIN = 1;
    public static float RATING_MAX = 5;

    public static int SPOT_NAME_MAX_LENGTH = 20;
    public static int SPOT_NAME_MIN_LENGTH = 3;
    public static int SPOT_DESCRIPTION_MAX_LENGTH = 140;
    public static int SPOT_DESCRIPTION_MIN_LENGTH = 0;
    public static int REPORT_MINIMUM_TO_DELETE_ACCOUNT = 50;
}
