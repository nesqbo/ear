package cz.cvut.fel.ear.date_database.controller;

import cz.cvut.fel.ear.date_database.model.Owner;
import cz.cvut.fel.ear.date_database.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Optional;

@RestController
public class TestController {
    @Autowired
    UserService userService;

    @GetMapping
    public String hello() {
        return
                "<h1>Hello world</h1>";
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String user(){
        return
                "<h1>Hello user</h1>";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    public String admin(){
        return
                "<h1>Hello admin</h1>";
    }

    @GetMapping("/owner")
    @PreAuthorize("hasRole('ROLE_OWNER')")
    public String owner(Principal principal){
        String name = principal.getName();
        Optional<Owner> owner = userService.findOwnerByUsername(name);
        if (owner.isPresent())
            return
                    String.format("<h1>Hello %s</h1>", owner.get().getName());
        else {
            return
                    "<h1>Hello owner</h1>";
        }
    }
}
