package cz.cvut.fel.ear.date_database.security;

import cz.cvut.fel.ear.date_database.model.Admin;
import cz.cvut.fel.ear.date_database.model.BaseUser;
import cz.cvut.fel.ear.date_database.model.Owner;
import cz.cvut.fel.ear.date_database.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

@Configurable
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    UserService userService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<BaseUser> user = userService.findByUsername(username);
        Optional<Admin> admin = userService.findAdminByUsername(username);
        Optional<Owner> owner = userService.findOwnerByUsername(username);
        if (user.isEmpty() && admin.isEmpty() && owner.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        } else {
            if (admin.isPresent())
                return new AdminUserDetails(admin.get());
            else if (owner.isPresent()){
                return new OwnerUserDetails(owner.get());
            } else {
                return new BaseUserDetails(user.get());
            }
        }
    }
}
