package cz.cvut.fel.ear.date_database.service.utils;

import cz.cvut.fel.ear.date_database.model.Admin;

public class DeleteSpotPrivilegeCheck implements AdminPrivilegesCheckStrategy {
    @Override
    public boolean authorized(Admin admin) {
        return admin.getAdminLevel().CAN_DELETE_SPOTS;
    }
}
