package cz.cvut.fel.ear.date_database.dao;

import cz.cvut.fel.ear.date_database.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> {
    Admin getAdminById(int id);
    Optional<Admin> findByUsername(String username);
}
