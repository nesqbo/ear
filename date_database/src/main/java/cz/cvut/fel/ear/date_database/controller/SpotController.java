package cz.cvut.fel.ear.date_database.controller;

import cz.cvut.fel.ear.date_database.exception.InvalidValueInRequestBodyException;
import cz.cvut.fel.ear.date_database.model.Spot;
import cz.cvut.fel.ear.date_database.service.SpotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.sql.SQLDataException;
import java.util.List;

@RestController
@RequestMapping("/v1/api/spots")
public class SpotController {
    @Autowired
    SpotService spotService;

    @GetMapping("/all")
    public ResponseEntity<List<Spot>> getAllSpots(){
            return ResponseEntity.ok(spotService.getAllSpots());
    }

    @GetMapping("/{spotId}")
    public ResponseEntity<Spot> getSpotById(@PathVariable int spotId){
        return ResponseEntity.ok(spotService.getSpotById(spotId));
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/add")
    public ResponseEntity<String> addSpot(@RequestBody Spot spot){
        try {
            spotService.addSpot(spot);
        } catch (InvalidValueInRequestBodyException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        return ResponseEntity.created(URI.create("/v1/api/spots/" + spot.getId())).body("Spot added successfully");

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteSpot(@RequestParam Integer id){
        if (id == 0){
            String errorMessage = "Spot id cannot be null";
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }
        try {
            spotService.deleteSpotById(id);
            return ResponseEntity.ok("Spot deleted successfully");
        } catch (SQLDataException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }

    }

    @PutMapping("/update")
    public ResponseEntity<String> updateSpot(@RequestBody Spot spot){
        if (spot.getId() == 0){
            String errorMessage = "Spot id cannot be null";
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }
        try {
            spotService.updateSpot(spot);
            return ResponseEntity.ok("Spot updated successfully");
        } catch (SQLDataException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PutMapping("/addSpotToCategory")
    public ResponseEntity<String> addSpotToCategory(@RequestBody int spotId, @RequestBody int categoryId) {
        if (spotId == 0 || categoryId == 0) {
            String errorMessage = "Spot and category cannot be null";
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }

        try {
            spotService.addSpotToCategory(spotId, categoryId);
            return ResponseEntity.ok("Spot added to category successfully");
        } catch (SQLDataException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }

    }
}
