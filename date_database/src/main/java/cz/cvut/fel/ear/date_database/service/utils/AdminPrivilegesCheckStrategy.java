package cz.cvut.fel.ear.date_database.service.utils;

import cz.cvut.fel.ear.date_database.model.Admin;

public interface AdminPrivilegesCheckStrategy {
    boolean authorized(Admin admin);
}
