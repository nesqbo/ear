package cz.cvut.fel.ear.date_database.dao;

import cz.cvut.fel.ear.date_database.model.Owner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Integer> {
    Owner findByIco(String ico);

    //criteriaApi findByICO


    Optional<Owner> findByUsername(String ownerUsername);
}
