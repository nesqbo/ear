package cz.cvut.fel.ear.date_database.exception;

public class InvalidIcoException extends RuntimeException {
    public InvalidIcoException() {
    }

    public InvalidIcoException(String message) {
        super(message);
    }
}
