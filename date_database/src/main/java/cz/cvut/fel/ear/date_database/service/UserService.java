package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.dao.AdminRepository;
import cz.cvut.fel.ear.date_database.dao.OwnerRepository;
import cz.cvut.fel.ear.date_database.dao.UserRepository;
import cz.cvut.fel.ear.date_database.exception.DateDBException;
import cz.cvut.fel.ear.date_database.exception.InvalidIcoException;
import cz.cvut.fel.ear.date_database.exception.UnauthorizedException;
import cz.cvut.fel.ear.date_database.model.Admin;
import cz.cvut.fel.ear.date_database.model.BaseUser;
import cz.cvut.fel.ear.date_database.model.Owner;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

@Service
public class UserService {
    private final AdminRepository adminRepository;
    private final OwnerRepository ownerRepository;
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[\\w-.]+@([\\w-]+\\.)+[\\w-]{2,4}$");

    @Autowired
    public UserService(AdminRepository adminRepository, OwnerRepository ownerRepository,
                       UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.adminRepository = adminRepository;
        this.ownerRepository = ownerRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    public List<BaseUser> getAllUsersOfAllTypes() {
        List<BaseUser> users = new ArrayList<>();
        users.addAll(adminRepository.findAll());
        users.addAll(ownerRepository.findAll());
        users.addAll(userRepository.findAll());
        return users;
    }


    public List<Admin> getAllAdmins() {
        return adminRepository.findAll();
    }


    public List<Owner> getAllOwners() {
        return ownerRepository.findAll();
    }


    public List<BaseUser> getAllBaseUsers() {
        return userRepository.findAll();
    }

    @SneakyThrows
    public void addOwner(Owner owner) {
        basicValidation(owner);
        if (!owner.getIco().matches("\\d+")){
            throw new InvalidIcoException("ICO must be a number");
        }

        if (owner.getIco().length() != 8) {
            throw new InvalidIcoException("ICO must be 8 characters long");
        }

        if (ownerRepository.findByIco(owner.getIco()) != null) {
            throw new InvalidIcoException("ICO must be unique");
        }

        owner.setAccountCreation(new Date());
        owner.setPassword(passwordEncoder.encode(owner.getPassword()));
        ownerRepository.save(owner);
    }

    @SneakyThrows
    public void addUser(BaseUser user) {
        basicValidation(user);
        user.setAccountCreation(new Date());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setDeleted(false);
        user.setReportsReceived(0);
        userRepository.save(user);
    }

    @SneakyThrows
    public void addAdmin(Admin admin) {
        if (admin.getAdminLevel() == null) {
            throw new DateDBException("Admin level must be set");
        }
        basicValidation(admin);
        admin.setAccountCreation(new Date());
        admin.setPassword(passwordEncoder.encode(admin.getPassword()));
        adminRepository.save(admin);
    }

    @SneakyThrows
    public int viewUsersReportsReceived(int userId) throws UnauthorizedException {
        if (userId == 0) {
            throw new DateDBException("Invalid id");
        }

        if (userRepository.findById(userId).isPresent()) {
            return userRepository.findById(userId).get().getReportsReceived();
        } else {
            throw new DateDBException("User with id " + userId + " does not exist");
        }
    }

    public boolean authenticateUser(String username, String encodedPassword) throws DateDBException {
        if (userRepository.findByUsername(username).isPresent()){
            return Objects.equals(
                    encodedPassword,
                    userRepository.findByUsername(username).get().getPassword());
        } else {
            throw new DateDBException("User with username " + username + " does not exist");
        }
    }

    @SneakyThrows
    public void changePassword(BaseUser user, String oldPassword, String newPassword) {
        if (userRepository.findByUsername(user.getUsername()).isEmpty()) {
            throw new DateDBException("User with username " + user.getUsername() + " does not exist");
        }
        if (oldPassword == null || newPassword == null) {
            throw new DateDBException("Old and new password must be specified");
        }
        if (oldPassword.equals(newPassword)) {
            throw new DateDBException("New password must be different from the old one");
        }
        requireNonNull(oldPassword);
        requireNonNull(newPassword);
//        newPassword = encodePassword(newPassword);
//        oldPassword = encodePassword(oldPassword);
        if (authenticateUser(user.getUsername(), oldPassword)){
            user.setPassword(newPassword);
            return;
        }
        throw new DateDBException("wrong credentials");
    }

    private void basicValidation(BaseUser user) throws DateDBException {
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new DateDBException("Username must be unique");
        }
        if (userRepository.existsByEmail(user.getEmail())) {
            throw new DateDBException("Email must be unique");
        }
        if (!EMAIL_PATTERN.matcher(user.getEmail()).matches()) {
            throw new DateDBException("Email must be valid");
        }
        if (user.getUsername().length() < 3 || user.getUsername().length() > 20) {
            throw new DateDBException("Username must be between 3 and 20 characters");
        }
    }

    public Optional<BaseUser> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @SneakyThrows
    public Optional<Owner> findOwnerByUsername(String username) {
        return ownerRepository.findByUsername(username);
    }

    public Optional<Admin> findAdminByUsername(String username) {
        return adminRepository.findByUsername(username);
    }
}
