package cz.cvut.fel.ear.date_database.model;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor

public class Admin extends BaseUser {
    @Enumerated(EnumType.ORDINAL)
    private AdminLevel adminLevel;

    @AllArgsConstructor
    public enum AdminLevel {
        REVIEW_MANAGEMENT(1, false, true, false),
        SPOT_MANAGEMENT(2, false, false, true),
        SUPER_ADMIN(3, true, true, true);

        public final int ADMIN_LEVEL;
        public final boolean CAN_DELETE_ACCOUNTS;
        public final boolean CAN_DELETE_REVIEWS;
        public final boolean CAN_DELETE_SPOTS;
    }
}
