package cz.cvut.fel.ear.date_database.dao;

import cz.cvut.fel.ear.date_database.model.Owner;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public class CriteriaOwner {

    @PersistenceContext
    private EntityManager em;


    //using criteria api
    public Owner findByIco(String ico) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Owner> cq = cb.createQuery(Owner.class);
        final Root<Owner> owner = cq.from(Owner.class);

        cq.select(owner).where(
                cb.equal(owner.get("ico"), ico)
        );

        return em.createQuery(cq).getSingleResult();

    }

    public Optional<Owner> findByUsername(String ownerUsername) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Owner> cq = cb.createQuery(Owner.class);
        final Root<Owner> owner = cq.from(Owner.class);

        cq.select(owner).where(
                cb.equal(owner.get("username"), ownerUsername)
        );

        return Optional.of(em.createQuery(cq).getSingleResult());
    }
}
