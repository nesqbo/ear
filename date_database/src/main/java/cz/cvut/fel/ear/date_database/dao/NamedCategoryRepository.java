package cz.cvut.fel.ear.date_database.dao;

import cz.cvut.fel.ear.date_database.model.Category;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NamedCategoryRepository {


    //todo add to service and code overall
    @PersistenceContext
    private EntityManager em;

    public List<Category> findAll() {
        return em.createNamedQuery("Category.findAll", Category.class).getResultList();
    }

    public List<Category> findByName(String name) {
        return em.createNamedQuery("Category.findByName", Category.class).setParameter("name", name).getResultList();
    }
}
