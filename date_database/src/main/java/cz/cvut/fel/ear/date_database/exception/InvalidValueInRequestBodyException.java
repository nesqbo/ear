package cz.cvut.fel.ear.date_database.exception;

public class InvalidValueInRequestBodyException extends RuntimeException {
    public InvalidValueInRequestBodyException(String message) {
        super(message);
    }
}
