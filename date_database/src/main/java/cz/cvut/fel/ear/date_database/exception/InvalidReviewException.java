package cz.cvut.fel.ear.date_database.exception;

public class InvalidReviewException extends RuntimeException{
    public InvalidReviewException(String message) {
        super(message);
    }
}
