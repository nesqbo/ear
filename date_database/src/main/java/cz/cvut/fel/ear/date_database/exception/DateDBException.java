package cz.cvut.fel.ear.date_database.exception;

public class DateDBException extends Throwable {
    public DateDBException() {
    }

    public DateDBException(String message) {
        super(message);
    }
}
