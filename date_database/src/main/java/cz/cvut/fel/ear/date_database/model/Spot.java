package cz.cvut.fel.ear.date_database.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Spot {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(nullable = false)
    private String coordinates;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "owner_id", nullable = false)
    private Owner owner;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "Spot_has_Category",
            joinColumns = @JoinColumn(name = "spot_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    @OrderBy("name")
    private List<Category> categories;

    @Transient
    private float reviewAverage;

    @OneToMany(mappedBy = "spot")
    private List<Review> reviews;

//    public float getReviewAverage() {
//        float sum = 0;
//        for (Review review : reviews) {
//            sum += review.getRating();
//        }
//        return sum / reviews.size();
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Spot spot = (Spot) o;

        if (!coordinates.equals(spot.coordinates)) return false;
        return name.equals(spot.name);
    }

    @Override
    public int hashCode() {
        int result = coordinates.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    public List<Category> getCategories() {
        if (categories == null) {
            categories = List.of();
        }
        return categories;
    }
}
