package cz.cvut.fel.ear.date_database.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private float rating;

    @Column(nullable = false)
    private Date date;

    @Column(nullable = false)
    private boolean deleted;

    private int reportsReceived;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private BaseUser user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "spot_id", nullable = false)
    private Spot spot;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Review review = (Review) o;

        if (!title.equals(review.title)) return false;
        if (!user.equals(review.user)) return false;
        return spot.equals(review.spot);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + user.hashCode();
        result = 31 * result + spot.hashCode();
        return result;
    }
}
