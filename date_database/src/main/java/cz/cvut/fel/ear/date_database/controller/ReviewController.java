package cz.cvut.fel.ear.date_database.controller;

import cz.cvut.fel.ear.date_database.exception.UnauthorizedException;
import cz.cvut.fel.ear.date_database.model.Review;
import cz.cvut.fel.ear.date_database.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;
import java.util.List;
import java.util.Objects;


@RestController
@RequestMapping("/v1/api/reviews")
public class ReviewController {
    @Autowired
    ReviewService reviewService;

    @GetMapping("/spot/{id}")
    public ResponseEntity<List<Review>> getAllReviewsBasedOnSpotId(@RequestParam int id){
        return ResponseEntity.ok(reviewService.getAllReviewsBasedOnSpotId(id));
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/add")
    public ResponseEntity<String> addReview(@RequestBody Review review){
        return ResponseEntity.created(URI.create("/v1/api/spot/" + review.getSpot().getId())).body("Review added successfully");
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @DeleteMapping("/delete/{reviewId}")
    public void deleteReview(@RequestParam int reviewId, Principal principal){
        Review reviewToDelete = reviewService.getReviewById(reviewId);
        if (principal == null || !Objects.equals(reviewToDelete.getUser().getUsername(), principal.getName())){
            throw new UnauthorizedException("User is not authorized to delete this review");
        }
        reviewService.deleteReviewById(reviewId);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @DeleteMapping("/update/{reviewId}")
    public void updateReview(@RequestBody Review review, @RequestParam int reviewId, Principal principal){
        Review reviewToUpdate = reviewService.getReviewById(reviewId);
        if (principal == null || !Objects.equals(reviewToUpdate.getUser().getUsername(), principal.getName())){
            throw new UnauthorizedException("User is not authorized to update this review");
        }
        reviewService.updateReview(review);
    }

}
