package cz.cvut.fel.ear.date_database.service.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Date;

@Slf4j
public class UtilMethods {
    private static final String PASSWORD_SALT = "ledvinka";


    public static Date addMinutes(Date date, int minutes) {
        long timeInMillis = date.getTime();
        long addedMilliseconds = minutes * 60000L; // 1 minute = 60,000 milliseconds
        return new Date(timeInMillis + addedMilliseconds);
    }

    public static String encodePassword(String password) {
        final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.encode(PASSWORD_SALT + password);
    }
}
