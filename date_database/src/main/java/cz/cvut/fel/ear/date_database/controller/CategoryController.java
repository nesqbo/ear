package cz.cvut.fel.ear.date_database.controller;

import cz.cvut.fel.ear.date_database.model.Category;
import cz.cvut.fel.ear.date_database.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/api/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("/all")
    public ResponseEntity<List<Category>> findAll() {
        return ResponseEntity.ok(categoryService.findAllCategories());
    }

    @GetMapping("/{categoryId}")
    public ResponseEntity<Category> findById(@RequestParam int categoryId) {
        return ResponseEntity.ok(categoryService.findCategoryById(categoryId));
    }
}
