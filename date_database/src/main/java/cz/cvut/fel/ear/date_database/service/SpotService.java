package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.dao.ReviewRepository;
import cz.cvut.fel.ear.date_database.exception.InvalidValueInRequestBodyException;
import cz.cvut.fel.ear.date_database.dao.CategoryRepository;
import cz.cvut.fel.ear.date_database.dao.SpotRepository;
import cz.cvut.fel.ear.date_database.model.Category;
import cz.cvut.fel.ear.date_database.model.Review;
import cz.cvut.fel.ear.date_database.model.Spot;
import jakarta.persistence.PersistenceException;
import jakarta.transaction.Transactional;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLDataException;
import java.util.List;
import java.util.Optional;

import static cz.cvut.fel.ear.date_database.service.utils.ConstantValues.*;

@Service
public class SpotService {
    private final SpotRepository spotRepository;
    private final CategoryRepository categoryRepository;
    private final ReviewRepository reviewRepository;
    @Autowired
    public SpotService(SpotRepository spotRepository, CategoryRepository categoryRepository, ReviewRepository reviewRepository) {
        this.spotRepository = spotRepository;
        this.categoryRepository = categoryRepository;
        this.reviewRepository = reviewRepository;
    }

    public List<Spot> getAllSpots(){
        return spotRepository.findAllByDeleted(false);
    }

    @Transactional
    public void addSpot(Spot spot){
        try {
            if (spot.getName().length() > SPOT_NAME_MAX_LENGTH || spot.getName().length() < SPOT_NAME_MIN_LENGTH){
                throw new InvalidValueInRequestBodyException(String.format("Name must be between %d and %d characters", SPOT_NAME_MIN_LENGTH, SPOT_NAME_MAX_LENGTH));
            }
            if (spotRepository.existsByName(spot.getName())){
                throw new InvalidValueInRequestBodyException("Spot with name " + spot.getName() + " already exists");
            }
            if (spot.getDescription() == null || spot.getDescription().length() < SPOT_DESCRIPTION_MIN_LENGTH
                    || spot.getDescription().length() > SPOT_DESCRIPTION_MAX_LENGTH){
                throw new InvalidValueInRequestBodyException(String.format("Description must be between %d and %d characters",
                        SPOT_DESCRIPTION_MIN_LENGTH, SPOT_DESCRIPTION_MAX_LENGTH));
            }

            spotRepository.save(spot);
        } catch (PersistenceException e) {
            throw new InvalidValueInRequestBodyException("There was a problem with inserting your spot");
        }
    }

    @SneakyThrows
    public Spot getSpotById(Integer id) {
        Optional<Spot> spot = spotRepository.findById(id);
        if (spot.isEmpty() || spot.get().isDeleted()) {
            throw new InvalidValueInRequestBodyException("Spot with id " + id + " does not exist");
        }

        spot.get().setReviewAverage(calculateAverageSpotReview(id));
        return spot.get();
    }

    private float calculateAverageSpotReview(int spotId){
        List<Review> reviews = reviewRepository.findAllWithoutDeleted();
        reviews.removeIf(review -> review.getSpot().getId() != spotId);
        float sum = 0;

        if (reviews.size() == 0){
            return 0;
        }

        if (reviews.size() <= 10){
            for (Review review : reviews){
                sum += review.getRating();
            }
            return sum / reviews.size();
        } else {
            int excessFromDivisionByTen = reviews.size() % 10;
            int howManyReviewsIWantToCalculate = reviews.size() - excessFromDivisionByTen;
            for (int i = 0; i < howManyReviewsIWantToCalculate; i++){
                sum += reviews.get(i).getRating();
            }
            return sum / howManyReviewsIWantToCalculate;
        }
    }

    public void updateSpot(Spot spot) throws SQLDataException {
        Optional<Spot> spotFromDB = spotRepository.findById(spot.getId());
        if (spotFromDB.isEmpty() || spotFromDB.get().isDeleted()){
            throw new InvalidValueInRequestBodyException("Spot does not exist");
        }
        spotRepository.save(spot);
    }

    public void deleteSpotById(Integer id) throws SQLDataException {
        if (!spotRepository.existsById(id)){
            throw new SQLDataException("Spot with id " + id + " does not exist");
        }
        Spot spot = spotRepository.getSpotById(id);
        spot.setDeleted(true);
        spotRepository.save(spot);
    }

    public void addSpotToCategory(int spotId, int categoryId) throws SQLDataException {
        Optional<Spot> spot = spotRepository.findById(spotId);
        Optional<Category> category = categoryRepository.findById(categoryId);
        if (spot.isEmpty() || category.isEmpty() || spot.get().isDeleted() || category.get().isDeleted()){
            throw new SQLDataException("Spot or category does not exist");
        }
        spot.get().getCategories().add(category.get());
        spotRepository.save(spot.get());
    }

    public void removeSpotFromCategory(int spotId, int categoryId) throws SQLDataException {
        Optional<Spot> spot = spotRepository.findById(spotId);
        Optional<Category> category = categoryRepository.findById(categoryId);
        if (spot.isEmpty() || category.isEmpty() || spot.get().isDeleted() || category.get().isDeleted()){
            throw new SQLDataException("Spot or category does not exist");
        }
        List<Category> categories = spot.get().getCategories();
        if (!categories.contains(category.get())){
            throw new InvalidValueInRequestBodyException("Spot does not contain this category");
        }
        categories.remove(category.get());
        spot.get().setCategories(categories);
        spotRepository.save(spot.get());
    }
}
