package cz.cvut.fel.ear.date_database.controller;

import cz.cvut.fel.ear.date_database.model.Category;
import cz.cvut.fel.ear.date_database.service.AdminActionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;


@RestController
@RequestMapping("/v1/api/admin")
public class AdminController {
    @Autowired
    AdminActionsService adminActionsService;

    @PreAuthorize("hasAnyRole('ROLE_REVIEW_MANAGEMENT_ADMIN', 'ROLE_SUPER_ADMIN')")
    @DeleteMapping("/review/{reviewId}")
    public ResponseEntity<?> deleteReview(@PathVariable int reviewId) {
        adminActionsService.deleteReview(reviewId);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    @DeleteMapping("/user/{username}")
    public ResponseEntity<?> deleteUser(@PathVariable String username) {
        adminActionsService.deleteUser(username);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ROLE_SPOT_MANAGEMENT_ADMIN', 'ROLE_SUPER_ADMIN')")
    @DeleteMapping("/spot/{spotId}")
    public ResponseEntity<?> deleteSpot(@PathVariable int spotId) {
        adminActionsService.deleteSpot(spotId);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    @DeleteMapping("/owner/{username}")
    public ResponseEntity<?> deleteOwner(@PathVariable String username) {
        adminActionsService.deleteOwner(username);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ROLE_SPOT_MANAGEMENT_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_REVIEW_MANAGEMENT_ADMIN')")
    @PostMapping("/category")
    public ResponseEntity<?> addCategory(@RequestBody Category category) {
        adminActionsService.addCategory(category);
        return ResponseEntity.created(URI.create("/v1/api/category/" + category.getId())).body("Category added successfully");
    }

    @PreAuthorize("hasAnyRole('ROLE_SPOT_MANAGEMENT_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_REVIEW_MANAGEMENT_ADMIN')")
    @DeleteMapping("/category/{categoryId}")
    public ResponseEntity<?> deleteCategory(@PathVariable int categoryId) {
        adminActionsService.deleteCategory(categoryId);
        return ResponseEntity.noContent().build();
    }
}
