package cz.cvut.fel.ear.date_database.exception;

public class UnauthorizedException extends IllegalArgumentException{

    public UnauthorizedException() {
    }

    public UnauthorizedException(String message) {
        super(message);
    }
}
