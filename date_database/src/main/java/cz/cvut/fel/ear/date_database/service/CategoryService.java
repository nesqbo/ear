package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.dao.CategoryRepository;
import cz.cvut.fel.ear.date_database.model.Category;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    public List<Category> findAllCategories(){
        return categoryRepository.findAll();
    }

    public Category findCategoryById(int categoryId){
        Optional<Category> category = categoryRepository.findById(categoryId);
        if (category.isEmpty()){
            throw new IllegalArgumentException("Category with id " + categoryId + " does not exist");
        }
        return category.get();
    }

    //ADD SPOT TO CATEGORY IS IN SPOT SERVICE
    //CREATING AND DELETING CATEGORIES IS IN ADMIN ACTIONS SERVICE
}
