package cz.cvut.fel.ear.date_database.controller;

import cz.cvut.fel.ear.date_database.model.Admin;
import cz.cvut.fel.ear.date_database.model.Owner;
import cz.cvut.fel.ear.date_database.model.BaseUser;
import cz.cvut.fel.ear.date_database.service.UserService;
import jakarta.persistence.PersistenceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/api/users")
@Slf4j
public class UserController {
    @Autowired
    UserService userService;

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_USER_MANAGEMENT_ADMIN')")
    @GetMapping("/all")
    public List<BaseUser> getAllUsersOfAllTypes() {
        return userService.getAllUsersOfAllTypes();
    }

    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    @GetMapping("/admin")
    public List<Admin> getAllAdmins() {
        return userService.getAllAdmins();
    }

    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    @GetMapping("/")
    public List<BaseUser> getAllUsers() {
        return userService.getAllUsersOfAllTypes();
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_USER_MANAGEMENT_ADMIN')")
    @GetMapping("/owner")
    public List<Owner> getAllOwners() {
        return userService.getAllOwners();
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_USER_MANAGEMENT_ADMIN')")
    @PostMapping("/owner/add")
    public ResponseEntity<String> addOwner(@RequestBody Owner owner) {
        if (owner.getUsername() == null || owner.getPassword() == null || owner.getName() == null
                || owner.getIco() == null || owner.getEmail() == null) {
            return ResponseEntity.badRequest().body("Username, password, name, ICO and email must be specified");
        }

        try {
            userService.addOwner(owner);
            return ResponseEntity.created(URI.create("/api/user/owner/" + owner.getUsername())).body("Owner added successfully");
        } catch (PersistenceException e) {
            log.error("There was a problem with adding the owner", e);
            return ResponseEntity.badRequest().body("There was a problem with adding the owner");
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_USER_MANAGEMENT_ADMIN')")
    @GetMapping("/owner/{username}")
    public ResponseEntity<Owner> getOwnerByUsername(@PathVariable String username) {
        Optional<Owner> owner = userService.findOwnerByUsername(username);
        return owner.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/add")
    public ResponseEntity<String> addUser(@RequestBody BaseUser user) {
        if (user.getUsername() == null || user.getPassword() == null || user.getEmail() == null) {
            return ResponseEntity.badRequest().body("Username, password, email must be specified");
        }

        try {
            userService.addUser(user);
            return ResponseEntity.created(URI.create("/v1/api/user/" + user.getUsername())).body("User added successfully");
        } catch (PersistenceException e) {
            log.error("There was a problem with adding the user", e);
            return ResponseEntity.badRequest().body("There was a problem with adding the user");
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_USER_MANAGEMENT_ADMIN', 'ROLE_USER')")
    @GetMapping("/{username}")
    public ResponseEntity<BaseUser> getUserByUsername(@PathVariable String username) {
        Optional<BaseUser> user = userService.findByUsername(username);
        return user.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
}
