package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.dao.*;
import cz.cvut.fel.ear.date_database.model.*;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static cz.cvut.fel.ear.date_database.service.utils.ConstantValues.REPORT_MINIMUM_TO_DELETE_ACCOUNT;

@Service
@Slf4j

public class AdminActionsService {
    private final UserRepository userRepository;
    private final OwnerRepository ownerRepository;
    private final SpotRepository spotRepository;
    private final ReviewRepository reviewRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public AdminActionsService(UserRepository userRepository, OwnerRepository ownerRepository, SpotRepository spotRepository, ReviewRepository reviewRepository, CategoryRepository categoryRepository) {
        this.userRepository = userRepository;
        this.ownerRepository = ownerRepository;
        this.spotRepository = spotRepository;
        this.reviewRepository = reviewRepository;
        this.categoryRepository = categoryRepository;
    }

    public void deleteReview(int reviewId) {
        log.info("Trying to delete a review with id " + reviewId);

        Optional<Review> review = reviewRepository.findById(reviewId);
        if (review.isPresent()) {
            review.get().setDeleted(true);
            reviewRepository.save(review.get());
        } else {
            log.warn("Tried to delete a review with id " + reviewId + " but it does not exist");
            throw new IllegalArgumentException("Review with id " + reviewId + " does not exist");
        }
    }

    @Transactional
    public void deleteUser(String username) {
        log.info("Trying to delete a user with username " + username);

        Optional<BaseUser> userFromDB = userRepository.findByUsername(username);
        if (userFromDB.isPresent()) {
            if (userFromDB.get().getReportsReceived() >= REPORT_MINIMUM_TO_DELETE_ACCOUNT) {
                userFromDB.get().setDeleted(true);

                List<Review> reviews = reviewRepository.findAllByUser(userFromDB.get());
                for (Review review : reviews) {
                    review.setDeleted(true);
                }
                reviewRepository.saveAll(reviews);
                userRepository.save(userFromDB.get());
            } else {
                log.warn("Tried to delete a user with username " + username + " but it has not received enough reports to be deleted");
                throw new IllegalArgumentException("User " + username + " has not received enough reports to be deleted");
            }
        } else {
            log.warn("Tried to delete a user with username " + username + " but it does not exist");
            throw new IllegalArgumentException("User " + username +  " does not exist");
        }
    }

    public void deleteSpot(int spotId) {
        log.info("Trying to delete a spot with id " + spotId);

        Optional<Spot> spotFromDB = spotRepository.findById(spotId);
        if (spotFromDB.isPresent()) {
            spotFromDB.get().setDeleted(true);
            spotRepository.save(spotFromDB.get());
        } else {
            log.warn("Tried to delete a spot with id " + spotId + " but it does not exist");
            throw new IllegalArgumentException("Spot with id " + spotId + " does not exist");
        }
    }

    @Transactional
    public void deleteOwner(String ownerUsername) {
        log.info("Trying to delete an owner with username " + ownerUsername);

        Optional<Owner> owner = ownerRepository.findByUsername(ownerUsername);
        if (owner.isPresent()) {
            if (owner.get().getReportsReceived() >= REPORT_MINIMUM_TO_DELETE_ACCOUNT) {
                List<Spot> spots = spotRepository.findAllByOwner(owner.get());
                for (Spot spot : spots) {
                    spot.setDeleted(true);
                }
                spotRepository.saveAll(spots);
                owner.get().setDeleted(true);
                userRepository.save(owner.get());
            } else {
                log.warn("Tried to delete an owner with username " + ownerUsername + " but it has not received enough reports to be deleted");
                throw new IllegalArgumentException("Owner " + ownerUsername + " has not received enough reports to be deleted");
            }
        } else {
            log.warn("Tried to delete an owner with username " + ownerUsername + " but it does not exist");
            throw new IllegalArgumentException("Owner " + ownerUsername + " does not exist");
        }
    }

    public void addCategory(Category category) {
        if (categoryRepository.existsByName(category.getName())) {
            log.warn("Tried to add a category with name " + category.getName() + " but it already exists");
            throw new IllegalArgumentException("Category with name " + category.getName() + " already exists");
        }
        categoryRepository.save(category);
    }

    @Transactional
    public void deleteCategory(int categoryId) {
        log.info("Trying to delete a category with id " + categoryId);

        Optional<Category> category = categoryRepository.findById(categoryId);
        if (category.isPresent()) {
            if (category.get().isDeleted()){
                log.warn("Tried to delete a category with id " + categoryId + " but it is already deleted");
                throw new IllegalArgumentException("Category with id " + categoryId + " is already deleted");
            }
            category.get().setDeleted(true);
            List<Spot> spots = spotRepository.findByCategory(category.get());
            for (Spot spot : spots) {
                spot.getCategories().remove(category.get());
            }
            spotRepository.saveAll(spots);
            categoryRepository.save(category.get());
        } else {
            log.warn("Tried to delete a category with id " + categoryId + " but it does not exist");
            throw new IllegalArgumentException("Category with id " + categoryId + " does not exist");
        }
    }
}
