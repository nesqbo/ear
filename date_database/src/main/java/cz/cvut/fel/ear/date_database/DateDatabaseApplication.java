package cz.cvut.fel.ear.date_database;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DateDatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(DateDatabaseApplication.class, args);
    }

}
