package cz.cvut.fel.ear.date_database.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
public class BaseUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(unique = true, nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;
    private int reportsReceived;
    @Column(nullable = false)
    private Date accountCreation;
    private boolean deleted;
    @Column(unique = true, nullable = false)
    private String email;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseUser user = (BaseUser) o;

        if (!username.equals(user.username)) return false;
        if (!password.equals(user.password)) return false;
        if (!accountCreation.equals(user.accountCreation)) return false;
        return email.equals(user.email);
    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + accountCreation.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
