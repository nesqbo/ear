package cz.cvut.fel.ear.date_database.dao;

import cz.cvut.fel.ear.date_database.model.Category;
import cz.cvut.fel.ear.date_database.model.Owner;
import cz.cvut.fel.ear.date_database.model.Spot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpotRepository extends JpaRepository<Spot, Integer> {
    Spot getSpotById(int id);
    boolean existsByName(String name);

    List<Spot> findAllByDeleted(boolean deleted);

    List<Spot> findAllByOwner(Owner owner);

    @Query("SELECT s FROM Spot s WHERE :category MEMBER OF s.categories AND s.deleted = false")
    List<Spot> findByCategory(Category category);
}
