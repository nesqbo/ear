package cz.cvut.fel.ear.date_database.dao;

import cz.cvut.fel.ear.date_database.model.BaseUser;
import cz.cvut.fel.ear.date_database.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Integer> {

    @Query("SELECT r FROM Review r WHERE r.spot.id = ?1 AND r.deleted = false")
    List<Review> findReviewsBySpotId(int id);
    @Query("SELECT r FROM Review r WHERE r.spot.id = ?1")
    List<Review> findReviewsBySpotIdIncludeDeleted(int id);
    Review getReviewById(int id);
    List<Review> findAllByUser(BaseUser user);
    @Query("SELECT r FROM Review r WHERE r.deleted = false")
    List<Review> findAllWithoutDeleted();


    @Query("SELECT r FROM Review r WHERE r.spot.id = ?1 AND r.deleted = false")
    List<Review> findAllBySpotIdExcludeDeleted(int spotId);
}
