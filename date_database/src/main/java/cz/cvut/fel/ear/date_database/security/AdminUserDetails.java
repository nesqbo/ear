package cz.cvut.fel.ear.date_database.security;

import cz.cvut.fel.ear.date_database.model.Admin;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AdminUserDetails implements UserDetails {
    private final Admin admin;

    public AdminUserDetails(Admin admin) {
        this.admin = admin;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>(List.of(new SimpleGrantedAuthority("ROLE_USER")));
        if (admin.getAdminLevel().ADMIN_LEVEL == 1) {
            authorities.add(new SimpleGrantedAuthority("ROLE_REVIEW_MANAGEMENT_ADMIN"));
        } else if (admin.getAdminLevel().ADMIN_LEVEL == 2) {
            authorities.add(new SimpleGrantedAuthority("ROLE_SPOT_MANAGEMENT_ADMIN"));
        } else if (admin.getAdminLevel().ADMIN_LEVEL == 3) {
            authorities.add(new SimpleGrantedAuthority("ROLE_SUPER_ADMIN"));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return admin.getPassword();
    }

    @Override
    public String getUsername() {
        return admin.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
