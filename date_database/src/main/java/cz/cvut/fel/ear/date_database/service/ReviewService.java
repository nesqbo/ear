package cz.cvut.fel.ear.date_database.service;

import cz.cvut.fel.ear.date_database.exception.InvalidReviewException;
import cz.cvut.fel.ear.date_database.exception.UnauthorizedException;
import cz.cvut.fel.ear.date_database.dao.*;
import cz.cvut.fel.ear.date_database.model.Review;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static cz.cvut.fel.ear.date_database.service.utils.UtilMethods.addMinutes;
import static cz.cvut.fel.ear.date_database.service.utils.ConstantValues.*;

@Service
public class ReviewService {

    ReviewRepository reviewRepository;
    SpotRepository spotRepository;
    UserRepository userRepository;
    @Autowired
    public ReviewService(ReviewRepository reviewRepository, SpotRepository spotRepository, UserRepository userRepository){
        this.reviewRepository = reviewRepository;
        this.spotRepository = spotRepository;
        this.userRepository = userRepository;
    }

    @SneakyThrows
    public void deleteReviewById(int reviewId) throws UnauthorizedException {
        Review review = reviewRepository.getReviewById(reviewId);
        review.setDeleted(true);
        reviewRepository.save(review);
    }

    public void updateReview(Review review){
        reviewRepository.save(review);
    }

    public List<Review> getAllReviewsBasedOnSpotId(int id){
        if (spotRepository.getSpotById(id) == null){
            throw new InvalidReviewException("Spot does not exist");
        }
        return reviewRepository.findAllBySpotIdExcludeDeleted(id);
    }


    public void reviewPlace(Review review){
        Date dateOfCreation = review.getDate();
        Date datePlusFiveMinutes = addMinutes(new Date(), 5);
        Date dateMinusFiveMinutes = addMinutes(new Date(), -5);
        if (!basicReviewCreationCheck(review.getUser().getId(), review.getSpot().getId(), review.getRating(),
                review.getTitle(), review.getContent())){
            throw new InvalidReviewException("User is not allowed to review this spot");
        }
        if (dateOfCreation.after(datePlusFiveMinutes) || dateOfCreation.before(dateMinusFiveMinutes)){
            throw new InvalidReviewException("Review time is invalid");
        }
        reviewRepository.save(review);
    }

    private boolean basicReviewCreationCheck(int reviewerId, int spotId, float rating, String title, String content){
        //BUSINESS RULE - ADMINS AND OWNERS CANNOT REVIEW SPOTS

//        if (ownerRepository.getOwnerById(reviewerId) != null){
//            throw new UnauthorizedException("Owners and admins cannot review spots");
//        }
        if (rating < RATING_MIN || rating > RATING_MAX){//todo make own exception
            throw new InvalidReviewException("Rating must be between 1 and 5");
        }
        if (title.length() >= TITLE_MAX_LENGTH || content.length() >= CONTENT_MAX_LENGTH){//todo make own exception
            throw new InvalidReviewException("Title must be less than 50 characters");
        }
        if (userRepository.findById(reviewerId).isEmpty() || spotRepository.getSpotById(spotId) == null){
            throw new UnauthorizedException("User does not exist");
        }
        if (reviewRepository.findReviewsBySpotId(spotId).stream().anyMatch(review -> review.getUser().getId()
                == reviewerId) && reviewRepository.findReviewsBySpotId(spotId).stream().anyMatch(review ->
                review.getDate().getTime() - new java.util.Date().getTime() < 7884000000L)){
            throw new InvalidReviewException("User has already reviewed this spot in the last 3 months.");
        }
        return true;
    }

    @SneakyThrows
    public void reportReview(int reviewId){
        Review review = reviewRepository.getReviewById(reviewId);
        if (review.isDeleted()){
            throw new InvalidReviewException("Review is already deleted");
        }
        review.setReportsReceived(review.getReportsReceived() + 1);
        if (review.getReportsReceived() >= 50) {
            review.setDeleted(true);
        }
        reviewRepository.save(review);
    }

    public Review getReviewById(int id){
        Optional<Review> review = reviewRepository.findById(id);
        if (review.isEmpty()){
            throw new InvalidReviewException("Review does not exist");
        }
        return review.get();
    }

}
